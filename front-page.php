<?php require "functions.php" ?>
<!DOCTYPE html>
<html lang="en-US">
  <head>
    <meta charset="utf-8">
    <meta name="creator" content="This theme was created by the brazilian company DR Estúdio &ndash; https://danielrothier.com">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1">

    <title>Suppama</title>

    <link rel="stylesheet" href="styles/front-page/front-page-min.css">

    <script src="scripts/front-page/front-page.js" type="module"></script>
  </head>
  <body>
    <header id="top-header">
      <div class="menu">
        <div class="content-container">
          <picture class="logo">
            <img src="multimedia/images/small/s-logo-1.png" alt="Suppama Logo">
          </picture>
          <nav class="links-container">
            <ul class="pages-list">
              <li><a class="indoor" href="#">Indoor</a></li>
              <li><a class="outdoor" href="#">Outdoor</a></li>
              <li><a class="training" href="#">Training</a></li>
              <li class="nests-list"><a class="products" href="#">Products</a></li>
              <li><a class="join" href="#">Be a Part of Suppama</a></li>
              <li><a class="contact" href="#">Contact Us</a></li>
            </ul>
            <ul class="idioms-list">
              <li class="en">En</li>
            </ul>
          </nav>
          <div class="button-set">
            <button type="button" name="top-header-instructors-login">
              <span class="content-container">
                <span class="content">Instructors Login</span>
              </span>
            </button>
            <button type="button" name="top-header-cart">
              <span class="content-container">
                <span class="image-set">
                  <picture class="cart">
                    <img src="multimedia/images/x-small/S-cart-1.png" alt="">
                  </picture>
                  <span class="circle">
                    <data class="quantity" value="0">0</data>
                  </span>
                </span>
                <span class="text-set">
                  <span class="name">Cart</span>
                  <data class="price" value="0">&euro;0,00</data>
                </span>
                <picture class="arrow">
                  <img src="multimedia/images/x-small/S-arr-1.png" alt="">
                </picture>
              </span>
            </button>
          </div>
          <nav class="loose-menu">
            <ul class="social-list">
              <li><a href="#"><img src="multimedia/images/x-small/S-facebook-1.png" alt="facebook"></a></li>
              <li><a href="#"><img src="multimedia/images/x-small/S-twitter-1.png" alt="twitter"></a></li>
              <li><a href="#"><img src="multimedia/images/x-small/S-instagram-1.png" alt="instagram"></a></li>
              <li><a href="#"><img src="multimedia/images/x-small/S-google-plus-1.png" alt="google-plus"></a></li>
            </ul>
          </nav>
        </div>
      </div>
      <div class="intro">
        <div class="content-container">
          <div class="text-container">
            <div class="heading-container">
              <hgroup class="heading-set">
                <h1 class="title">
                  <span class="line">Balance</span>
                  <span class="line">Your Life</span>
                </h1>
                <h2 class="subtitle">Our floating exercise mat.</h2>
              </hgroup>
            </div>
            <div class="text-set">
              <p class="text">
                Voluptate sed, commodi fames cumque vero ea viverra, vel eum dolor, modi quo aperiam fugit, dolores anim quaerat incididunt.
              </p>
            </div>
          </div>
          <picture class="side-image">
            <img src="multimedia/images/x-large/L-nature-2.jpg" alt="">
          </picture>
        </div>
      </div>
    </header>
    <main id="top-content">
      <section id="about-page">
        <div class="content-container">
          <picture class="side-image">
            <img src="multimedia/images/medium/m-person-1.jpg" alt="">
          </picture>
          <div class="text-container">
            <header class="heading-container">
              <h1 class="title">
                Provident rem exercitation
              </h1>
            </header>
            <div class="text-set">
              <p class="text">
                Ac nesciunt eleifend! Maxime dicta laboris? Explicabo, mattis dictum incididunt dolorem? Exercitationem aliquip sagittis? Illo, sollicitudin iusto pretium, sagittis.
                Inceptos dolores? Vulputate vestibulum, praesent, pharetra cillum pellentesque mus odio! Montes.
              </p>
              <blockquote class="highlight">
                <p class="text">Exercitationem aliquip sagittis fringillat vivamus accumsan vitaelo, sollicitudin iusto pretium, sagittis. Inceptos dolores.</p>
              </blockquote>
              <p class="text">
                Vehicula! Donec torquent metus autem duis, temporibus platea aperiam accumsan, quo.
                Rem numquam nostrum, pede consectetur, anim unde aliqua quisquam, eius exercitation congue lorem, nascetur senectus facilisis, tincidunt, laboris earum.
              </p>
            </div>
          </div>
          <div class="image-set">
            <picture class="swan">
              <img src="multimedia/images/medium/m-swan-1.png" alt="">
            </picture>
            <picture class="board">
              <img src="multimedia/images/large/l-board-1.png" alt="">
            </picture>
          </div>
        </div>
        <div class="circle"></div>
      </section>
      <section id="activities">
        <h2 hidden>Activities</h2>
        <section id="indoors">
          <div class="frame-container">
            <div class="content-container">
              <header class="heading-container">
                <div class="circle">
                  <picture class="ilustration">
                    <img src="multimedia/images/x-small/S-ilustration-1.png" alt="">
                  </picture>
                </div>
                <h3 class="title">Indoors</h3>
              </header>
            </div>
            <section class="fitmat">
              <div class="content-container">
                <div class="content-body">
                  <header class="heading-container">
                    <hgroup class="heading-set">
                      <h4 class="title">Fitmat</h4>
                      <h5 class="subtitle">Our floating exercise mat.</h5>
                    </hgroup>
                  </header>
                  <div class="text-set">
                    <p class="text">
                      Ac nesciunt eleifend! Maxime dicta laboris? Explicabo, mattis dictum incididunt dolorem? Exercitationem aliquip sagittis. Illo, sollicitudin iusto pretium, sagittis.
                      Inceptos dolores? Vulputate vestibulum, praesent, pharetra cillum pellentesque.
                    </p>
                  </div>
                  <footer class="anchors-container">
                    <a class="video-anchor" href="#">
                      <picture class="video-post">
                        <img src="multimedia/images/medium/m-person-2.jpg" alt="Fitmap Video">
                      </picture>
                      <div class="circle">
                        <div class="arrow"></div>
                      </div>
                    </a>
                    <button type="button" name="fitmat-know-more">
                      <span class="content-container">
                        <div class="circle"></div>
                        <span class="call">Know More</span>
                      </span>
                    </button>
                  </footer>
                </div>
                <div class="side-image">
                  <picture class="cellphone">
                    <img src="multimedia/images/medium/m-cellphone-1.png" alt="">
                  </picture>
                  <picture class="splash">
                    <img src="multimedia/images/medium/m-splash-1.png" alt="">
                  </picture>
                </div>
              </div>
            </section>
          </div>
        </section>
        <section id="outdoors">
          <div class="frame-container">
            <div class="content-container">
              <header class="heading-container">
                <div class="circle">
                  <picture class="ilustration">
                    <img src="multimedia/images/x-small/S-ilustration-2.png" alt="">
                  </picture>
                </div>
                <h3 class="title">Outdoors</h3>
              </header>
            </div>
            <section class="section-block polo">
              <div class="content-container">
                <div class="content-body">
                  <header class="heading-container">
                    <hgroup class="heading-set">
                      <h4 class="title">Polo</h4>
                      <h5 class="subtitle">Our floating exercise mat.</h5>
                    </hgroup>
                  </header>
                  <div class="text-set">
                    <p class="text">
                      Ac nesciunt eleifend! Maxime dicta laboris? Explicabo, mattis dictum incididunt dolorem?
                      Exercitationem aliquip sagittis. Illo, sollicitudin iusto pretium, sagittis. Inceptos dolores? Vulputate vestibulum, praesent, pharetra cillum pellentesque.
                    </p>
                  </div>
                  <button type="button" name="polo-know-more">
                    <span class="content-container">
                      <div class="circle"></div>
                      <span class="call">Know More</span>
                    </span>
                  </button>
                </div>
                <picture class="side-image">
                  <img src="multimedia/images/medium/m-person-3.jpg" alt="">
                </picture>
              </div>
            </section>
            <section class="section-block yoga">
              <div class="content-container">
                <div class="content-body">
                  <header class="heading-container">
                    <hgroup class="heading-set">
                      <h4 class="title">Yoga</h4>
                      <h5 class="subtitle">Our floating exercise mat.</h5>
                    </hgroup>
                  </header>
                  <div class="text-set">
                    <p class="text">
                      Ac nesciunt eleifend! Maxime dicta laboris? Explicabo, mattis dictum incididunt dolorem?
                      Exercitationem aliquip sagittis. Illo, sollicitudin iusto pretium, sagittis. Inceptos dolores? Vulputate vestibulum, praesent, pharetra cillum pellentesque.
                    </p>
                  </div>
                  <button type="button" name="fitmat-know-more">
                    <span class="content-container">
                      <div class="circle"></div>
                      <span class="call">Know More</span>
                    </span>
                  </button>
                </div>
                <picture class="side-image">
                  <img src="multimedia/images/medium/m-person-4.jpg" alt="">
                </picture>
              </div>
            </section>
            <section class="outdoors-more">
              <div class="content-container">
                <section class="fitmap-workouts content-body">
                  <header class="heading-container">
                    <h4 class="title">Fitmat Workouts</h4>
                  </header>
                  <div class="text-set">
                    <p class="text">
                      Ac nesciunt eleifend! Maxime dicta laboris? Explicabo, mattis dictum incididunt dolorem?
                      Exercitationem aliquip sagittis. Illo, sollicitudin iusto pretium, sagittis. Inceptos dolores? Vulputate vestibulum, praesent, pharetra cillum pellentesque.
                    </p>
                  </div>
                  <button type="button" name="fitmat-know-more">
                    <span class="content-container content-body">
                      <div class="circle"></div>
                      <span class="call">Know More</span>
                    </span>
                  </button>
                </section>
                <section class="sup-tours content-body">
                  <header class="heading-container">
                    <h4 class="title">Sup Tours</h4>
                  </header>
                  <div class="text-set">
                    <p class="text">
                      Ac nesciunt eleifend! Maxime dicta laboris? Explicabo, mattis dictum incididunt dolorem?
                      Exercitationem aliquip sagittis. Illo, sollicitudin iusto pretium, sagittis. Inceptos dolores? Vulputate vestibulum, praesent, pharetra cillum pellentesque.
                    </p>
                  </div>
                  <button type="button" name="fitmat-know-more">
                    <span class="content-container">
                      <div class="circle"></div>
                      <span class="call">Know More</span>
                    </span>
                  </button>
                </section>
              </div>
            </section>
            <div class="loose-image">
              <picture class="swan">
                <img src="multimedia/images/medium/m-swan-1.png" alt="">
              </picture>
              <picture class="board">
                <img src="multimedia/images/large/l-board-2.png" alt="">
              </picture>
            </div>
          </div>
        </section>
        <section id="training">
          <div class="frame-container">
            <div class="content-container">
              <header class="heading-container">
                <div class="circle">
                  <picture class="ilustration">
                    <img src="multimedia/images/x-small/S-ilustration-3.png" alt="">
                  </picture>
                </div>
                <h3 class="title">Training</h3>
              </header>
            </div>
            <section class="instructor-training">
              <div class="content-container">
                <div class="content-body">
                  <header class="heading-container">
                    <hgroup class="heading-set">
                      <h4 class="title">
                        <span class="line">Instructor</span>
                        <span class="line">Training</span>
                      </h4>
                      <h5 class="subtitle">Our floating exercise mat.</h5>
                    </hgroup>
                  </header>
                  <div class="text-set">
                    <p class="text">
                      Ac nesciunt eleifend! Maxime dicta laboris? Explicabo, mattis dictum incididunt dolorem?
                      Exercitationem aliquip sagittis. Illo, sollicitudin iusto pretium, sagittis. Inceptos dolores? Vulputate vestibulum, praesent, pharetra cillum pellentesque.
                    </p>
                  </div>
                  <footer class="anchors-container">
                    <a class="video-anchor" href="#">
                      <picture class="video-post">
                        <img src="multimedia/images/medium/m-person-5.jpg" alt="Training video">
                      </picture>
                      <div class="circle">
                        <div class="arrow"></div>
                      </div>
                    </a>
                    <button type="button" name="fitmat-know-more">
                      <span class="content-container">
                        <div class="circle"></div>
                        <span class="call">Know More</span>
                      </span>
                    </button>
                  </footer>
                </div>
              </div>
            </section>
          </div>
          <aside class="instructor-call">
            <div class="content-container">
              <a class="banner-container" href="#">
                <picture class="ilustration">
                  <img src="multimedia/images/small/s-person-1.png" alt="">
                </picture>
                <h4 class="title">Become an Instructor Now!</h4>
                <div class="action-button">
                  <span class="content">Join Us</span>
                </div>
              </a>
            </div>
          </aside>
        </section>
        <section id="products">
          <div class="frame-container">
            <div class="content-container">
              <header class="heading-container">
                <div class="circle">
                  <picture class="ilustration">
                    <img src="multimedia/images/x-small/S-ilustration-4.png" alt="">
                  </picture>
                </div>
                <h3 class="title">Products</h3>
              </header>
              <ul class="advantages-list">
                <li>
                  <figure class="image-container">
                    <picture class="ilustration">
                      <img src="multimedia/images/x-small/S-ilustration-5.png" alt="">
                    </picture>
                    <figcaption class="description-container">
                      <h1 class="title">Best Shipping</h1>
                    </figcaption>
                  </figure>
                </li>
                <li>
                  <figure class="image-container">
                    <picture class="ilustration">
                      <img src="multimedia/images/x-small/S-ilustration-6.png" alt="">
                    </picture>
                    <figcaption class="description-container">
                      <h1 class="title">100&percnt; Warranty</h1>
                    </figcaption>
                  </figure>
                </li>
                <li>
                  <figure class="image-container">
                    <picture class="ilustration">
                      <img src="multimedia/images/x-small/S-ilustration-7.png" alt="">
                    </picture>
                    <figcaption class="description-container">
                      <h1 class="title">Secured Shop</h1>
                    </figcaption>
                  </figure>
                </li>
                <li>
                  <figure class="image-container">
                    <picture class="ilustration">
                      <img src="multimedia/images/x-small/S-ilustration-8.png" alt="">
                    </picture>
                    <figcaption class="description-container">
                      <h1 class="title">Fast Training</h1>
                    </figcaption>
                  </figure>
                </li>
              </ul>
              <section class="indoors product-category">
                <header class="heading-container">
                  <div class="circle">
                    <picture class="ilustration">
                      <img src="multimedia/images/x-small/S-ilustration-9.png" alt="">
                    </picture>
                  </div>
                  <h4 class="title">Indoors</h4>
                  <button type="button" name="indoors-all">
                    <span class="content-container">
                      <picture class="cart">
                        <img src="multimedia/images/x-small/S-cart-2.png" alt="">
                      </picture>
                      <span class="message">See All</span>
                    </span>
                  </button>
                </header>
                <ul class="products-row">
                  <li>
                    <section class="product">
                      <div class="images-block">
                        <picture class="product-image">
                          <img src="multimedia/images/medium/m-cellphone-2.png" alt="">
                        </picture>
                        <div class="circle">
                          <picture class="delivery-icon">
                            <img src="multimedia/images/x-small/S-truck-1.png" alt="">
                          </picture>
                        </div>
                      </div>
                      <span class="tags">Fitmaps, SUP Yoga</span>
                      <div class="text-block">
                        <h5 class="title">Inflatable Fitmat</h5>
                        <div class="info-set">
                          <div class="price-container">
                            <data class="previous-price" value=""></data>
                            <data class="current-price" value="645">
                              <span class="whole">645</span><span class="cents">.00</span>
                            </data>
                          </div>
                          <button type="button" name="cart-inflatable-fitmat">
                            <span class="content-container">
                              <picture class="cart">
                                <img src="multimedia/images/x-small/S-cart-1.png" alt="">
                              </picture>
                              <span class="add">Add to Cart</span>
                            </span>
                          </button>
                        </div>
                      </div>
                    </section>
                  </li>
                  <li>
                    <section class="product">
                      <div class="images-block">
                        <picture class="product-image">
                          <img src="multimedia/images/medium/m-shirt-1.png" alt="">
                        </picture>
                        <div class="circle">
                          <picture class="delivery-icon">
                            <img src="multimedia/images/x-small/S-truck-1.png" alt="">
                          </picture>
                        </div>
                      </div>
                      <span class="tags">Apparel, Accessories</span>
                      <div class="text-block">
                        <h5 class="title">
                          Short-Sleeved Windstop Shirt Iguana
                        </h5>
                        <div class="info-set">
                          <div class="price-container">
                            <data class="previous-price" value="57">
                              <span class="whole">57</span><span class="cents">.00</span>
                            </data>
                            <data class="current-price" value="49">
                              <span class="whole">49</span><span class="cents">.00</span>
                            </data>
                          </div>
                          <button type="button" name="cart-short-sleeved-windstop-shirt-iguana">
                            <span class="content-container">
                              <picture class="cart">
                                <img src="multimedia/images/x-small/S-cart-1.png" alt="">
                              </picture>
                              <span class="add">Add to Cart</span>
                            </span>
                          </button>
                        </div>
                      </div>
                    </section>
                    <div class="sale-box">
                      <div class="content-container">
                        <picture class="star">
                          <img src="multimedia/images/x-small/S-star-1.png" alt="">
                        </picture>
                        <span class="message">Sale</span>
                      </div>
                    </div>
                  </li>
                  <li>
                    <section class="product">
                      <div class="images-block">
                        <picture class="product-image">
                          <img src="multimedia/images/medium/m-cellphone-3.png" alt="">
                        </picture>
                      </div>
                      <span class="tags">Fitmats, SUP Yoga</span>
                      <div class="text-block">
                        <h5 class="title">Inflatable Fitmat Business Package</h5>
                        <div class="info-set">
                          <div class="price-container">
                            <data class="previous-price" value=""></data>
                            <data class="current-price" value="5750">
                              <span class="whole">5,750</span><span class="cents">.00</span>
                            </data>
                          </div>
                          <button type="button" name="cart-inflatable-fitmat-business-package">
                            <span class="content-container">
                              <picture class="cart">
                                <img src="multimedia/images/x-small/S-cart-1.png" alt="">
                              </picture>
                              <span class="add">Add to Cart</span>
                            </span>
                          </button>
                        </div>
                      </div>
                    </section>
                  </li>
                </ul>
              </section>
              <hr class="divisor">
              <section class="outdoors product-category">
                <header class="heading-container">
                  <div class="circle">
                    <picture class="ilustration">
                      <img src="multimedia/images/x-small/S-ilustration-10.png" alt="">
                    </picture>
                  </div>
                  <h4 class="title">Outdoors</h4>
                  <button type="button" name="outdoors-all">
                    <span class="content-container">
                      <picture class="cart">
                        <img src="multimedia/images/x-small/S-cart-3.png" alt="">
                      </picture>
                      <span class="message">See All</span>
                    </span>
                  </button>
                </header>
                <ul class="products-row">
                  <li>
                    <section class="product">
                      <div class="images-block">
                        <picture class="product-image">
                          <img src="multimedia/images/medium/m-board-1.png" alt="">
                        </picture>
                        <div class="circle">
                          <picture class="delivery-icon">
                            <img src="multimedia/images/x-small/S-truck-1.png" alt="">
                          </picture>
                        </div>
                      </div>
                      <span class="tags">Stand Up Paddling, SUP Boards</span>
                      <div class="text-block">
                        <h5 class="title">Suppama Go Touring Inflatable Sup Board</h5>
                        <div class="info-set">
                          <div class="price-container">
                            <data class="previous-price" value=""></data>
                            <data class="current-price" value="790">
                              <span class="whole">790</span><span class="cents">.00</span>
                            </data>
                          </div>
                          <button type="button" name="cart-suppama-go-touring-inflatable-sup-board">
                            <span class="content-container">
                              <picture class="cart">
                                <img src="multimedia/images/x-small/S-cart-1.png" alt="">
                              </picture>
                              <span class="add">Add to Cart</span>
                            </span>
                          </button>
                        </div>
                      </div>
                    </section>
                  </li>
                  <li>
                    <section class="product">
                      <div class="images-block">
                        <picture class="product-image">
                          <img src="multimedia/images/small/s-coat-1.png" alt="">
                        </picture>
                        <div class="circle">
                          <picture class="delivery-icon">
                            <img src="multimedia/images/x-small/S-truck-1.png" alt="">
                          </picture>
                        </div>
                      </div>
                      <span class="tags">Apparel, Accessories</span>
                      <div class="text-block">
                        <h5 class="title">Sea Kayaking Cag North Cape</h5>
                        <div class="info-set">
                          <div class="price-container">
                            <data class="previous-price" value="269">
                              <span class="whole">269</span><span class="cents">.00</span>
                            </data>
                            <data class="current-price" value="209">
                              <span class="whole">209</span><span class="cents">.99</span>
                            </data>
                          </div>
                          <button type="button" name="cart-sea-kayaking-cag-north-cape">
                            <span class="content-container">
                              <picture class="cart">
                                <img src="multimedia/images/x-small/S-cart-1.png" alt="">
                              </picture>
                              <span class="add">Add to Cart</span>
                            </span>
                          </button>
                        </div>
                      </div>
                    </section>
                    <div class="sale-box">
                      <div class="content-container">
                        <picture class="star">
                          <img src="multimedia/images/x-small/S-star-1.png" alt="">
                        </picture>
                        <span class="message">Sale</span>
                      </div>
                    </div>
                  </li>
                  <li>
                    <section class="product">
                      <div class="images-block">
                        <picture class="product-image">
                          <img src="multimedia/images/small/s-bag-1.png" alt="">
                        </picture>
                        <div class="circle">
                          <picture class="delivery-icon">
                            <img src="multimedia/images/x-small/S-truck-1.png" alt="">
                          </picture>
                        </div>
                      </div>
                      <span class="tags">Apparel, Accessories</span>
                      <div class="text-block">
                        <h5 class="title">Comfortable Life Jacket Swell With Zipper</h5>
                        <div class="info-set">
                          <div class="price-container">
                            <data class="previous-price" value=""></data>
                            <data class="current-price" value="85">
                              <span class="whole">85</span><span class="cents">.00</span>
                            </data>
                          </div>
                          <button type="button" name="cart-comfortable-life-jacket-swell-with-zipper">
                            <span class="content-container">
                              <picture class="cart">
                                <img src="multimedia/images/x-small/S-cart-1.png" alt="">
                              </picture>
                              <span class="add">Add to Cart</span>
                            </span>
                          </button>
                        </div>
                      </div>
                    </section>
                  </li>
                </ul>
              </section>
              <hr class="divisor">
              <section class="training product-category">
                <header class="heading-container">
                  <div class="circle">
                    <picture class="ilustration">
                      <img src="multimedia/images/x-small/S-ilustration-12.png" alt="">
                    </picture>
                  </div>
                  <h4 class="title">Instructor Training</h4>
                  <button type="button" name="training-all">
                    <span class="content-container">
                      <picture class="cart">
                        <img src="multimedia/images/x-small/S-cart-4.png" alt="">
                      </picture>
                      <span class="message">See All</span>
                    </span>
                  </button>
                </header>
                <ul class="products-row">
                  <li>
                    <section class="product">
                      <div class="images-block">
                        <picture class="product-image">
                          <img src="multimedia/images/small/s-person-2.png" alt="">
                        </picture>
                        <div class="circle">
                          <picture class="like-icon">
                            <img src="multimedia/images/x-small/S-like-1.png" alt="">
                          </picture>
                        </div>
                      </div>
                      <span class="tags">Instructor training</span>
                      <div class="text-block">
                        <h5 class="title">Suppama Hiit Instructor Training Bergen, Norway</h5>
                        <div class="info-set">
                          <div class="price-container">
                            <data class="previous-price" value=""></data>
                            <data class="current-price" value="299">
                              <span class="whole">299</span><span class="cents">.00</span>
                            </data>
                          </div>
                          <button type="button" name="cart-suppama-hiit-instructor-training-bergen">
                            <span class="content-container">
                              <picture class="cart">
                                <img src="multimedia/images/x-small/S-cart-1.png" alt="">
                              </picture>
                              <span class="add">Add to Cart</span>
                            </span>
                          </button>
                        </div>
                      </div>
                    </section>
                  </li>
                  <li>
                    <section class="product">
                      <div class="images-block">
                        <picture class="product-image">
                          <img src="multimedia/images/small/s-macbook-1.png" alt="">
                        </picture>
                        <div class="circle">
                          <picture class="clock-icon">
                            <img src="multimedia/images/x-small/S-clock-1.png" alt="">
                          </picture>
                        </div>
                      </div>
                      <span class="tags">Instructor training, Online</span>
                      <div class="text-block">
                        <h5 class="title">LMS Course</h5>
                        <div class="info-set">
                          <div class="price-container">
                            <data class="previous-price" value=""></data>
                            <data class="current-price" value="10">
                              <span class="whole">10</span><span class="cents">.00</span>
                            </data>
                          </div>
                          <button type="button" name="cart-lms-course">
                            <span class="content-container">
                              <picture class="cart">
                                <img src="multimedia/images/x-small/S-cart-1.png" alt="">
                              </picture>
                              <span class="add">Add to Cart</span>
                            </span>
                          </button>
                        </div>
                      </div>
                    </section>
                  </li>
                  <li>
                    <section class="product">
                      <div class="images-block">
                        <picture class="product-image">
                          <img src="multimedia/images/small/s-cellphone-1.png" alt="">
                        </picture>
                        <div class="circle">
                          <picture class="pdf-icon">
                            <img src="multimedia/images/x-small/S-pdf-1.png" alt="">
                          </picture>
                        </div>
                      </div>
                      <span class="tags">Instructor training, PDF</span>
                      <div class="text-block">
                        <h5 class="title">Suppama&reg; Yoga Instructor Training</h5>
                        <div class="info-set">
                          <div class="price-container">
                            <data class="previous-price" value=""></data>
                            <data class="current-price" value="10">
                              <span class="whole">10</span><span class="cents">.00</span>
                            </data>
                          </div>
                          <button type="button" name="cart-suppama-yoga-instructor-training">
                            <span class="content-container">
                              <picture class="cart">
                                <img src="multimedia/images/x-small/S-cart-1.png" alt="">
                              </picture>
                              <span class="add">Add to Cart</span>
                            </span>
                          </button>
                        </div>
                      </div>
                    </section>
                  </li>
                </ul>
              </section>
              <hr class="divisor">
              <section class="activities product-category">
                <header class="heading-container">
                  <div class="circle">
                    <picture class="ilustration">
                      <img src="multimedia/images/x-small/S-ilustration-11.png" alt="">
                    </picture>
                  </div>
                  <h4 class="title">Activities</h4>
                  <button type="button" name="activities-all">
                    <span class="content-container">
                      <picture class="cart">
                        <img src="multimedia/images/x-small/S-cart-5.png" alt="">
                      </picture>
                      <span class="message">See All</span>
                    </span>
                  </button>
                </header>
                <ul class="products-row">
                  <li>
                    <section class="product">
                      <div class="images-block">
                        <picture class="product-image">
                          <img src="multimedia/images/small/s-person-2.png" alt="">
                        </picture>
                        <div class="circle">
                          <picture class="like-icon">
                            <img src="multimedia/images/x-small/S-like-1.png" alt="">
                          </picture>
                        </div>
                      </div>
                      <span class="tags">Instructor training</span>
                      <div class="text-block">
                        <h5 class="title">Suppama Hiit Instructor Training Bergen, Norway</h5>
                        <div class="info-set">
                          <div class="price-container">
                            <data class="previous-price" value=""></data>
                            <data class="current-price" value="299">
                              <span class="whole">299</span><span class="cents">.00</span>
                            </data>
                          </div>
                          <button type="button" name="cart-suppama-hiit-instructor-training-bergen">
                            <span class="content-container">
                              <picture class="cart">
                                <img src="multimedia/images/x-small/S-cart-1.png" alt="">
                              </picture>
                              <span class="add">Add to Cart</span>
                            </span>
                          </button>
                        </div>
                      </div>
                    </section>
                  </li>
                  <li>
                    <section class="product">
                      <div class="images-block">
                        <picture class="product-image">
                          <img src="multimedia/images/small/s-macbook-1.png" alt="">
                        </picture>
                        <div class="circle">
                          <picture class="clock-icon">
                            <img src="multimedia/images/x-small/S-clock-1.png" alt="">
                          </picture>
                        </div>
                      </div>
                      <span class="tags">Instructor training, Online</span>
                      <div class="text-block">
                        <h5 class="title">LMS Course</h5>
                        <div class="info-set">
                          <div class="price-container">
                            <data class="previous-price" value=""></data>
                            <data class="current-price" value="10">
                              <span class="whole">10</span><span class="cents">.00</span>
                            </data>
                          </div>
                          <button type="button" name="cart-lms-course">
                            <span class="content-container">
                              <picture class="cart">
                                <img src="multimedia/images/x-small/S-cart-1.png" alt="">
                              </picture>
                              <span class="add">Add to Cart</span>
                            </span>
                          </button>
                        </div>
                      </div>
                    </section>
                  </li>
                  <li>
                    <section class="product">
                      <div class="images-block">
                        <picture class="product-image">
                          <img src="multimedia/images/small/s-cellphone-1.png" alt="">
                        </picture>
                        <div class="circle">
                          <picture class="pdf-icon">
                            <img src="multimedia/images/x-small/S-pdf-1.png" alt="">
                          </picture>
                        </div>
                      </div>
                      <span class="tags">Instructor training, PDF</span>
                      <div class="text-block">
                        <h5 class="title">Suppama&reg; Yoga Instructor Training</h5>
                        <div class="info-set">
                          <div class="price-container">
                            <data class="previous-price" value=""></data>
                            <data class="current-price" value="10">
                              <span class="whole">10</span><span class="cents">.00</span>
                            </data>
                          </div>
                          <button type="button" name="cart-suppama-yoga-instructor-training">
                            <span class="content-container">
                              <picture class="cart">
                                <img src="multimedia/images/x-small/S-cart-1.png" alt="">
                              </picture>
                              <span class="add">Add to Cart</span>
                            </span>
                          </button>
                        </div>
                      </div>
                    </section>
                  </li>
                </ul>
              </section>
            </div>
            <aside class="product-sales">
              <div class="content-container">
                <a class="banner-container" href="#">
                  <picture class="ilustration">
                    <img src="multimedia/images/small/s-coat-2.png" alt="">
                  </picture>
                  <h4 class="title">Best Product Sales</h4>
                  <div class="action-button">
                    <span class="content">Shop Now</span>
                  </div>
                </a>
              </div>
            </aside>
          </div>
        </section>
        <div id="miscellaneous">
          <div class="content-container">
            <section class="events">
              <header class="heading-container">
                <div class="circle">
                  <picture class="ilustration">
                    <img src="multimedia/images/x-small/S-calendar-2.png" alt="">
                  </picture>
                </div>
                <h3 class="title">Events</h3>
              </header>
              <div class="content-body">
                <div class="decorative-line"></div>
                <ul class="events-list">
                  <li>
                    <section class="event active">
                      <picture class="image-block">
                        <img src="multimedia/images/medium/m-person-2.jpg" alt="">
                      </picture>
                      <div class="text-block">
                        <time class="date" datetime="2018-05-29">
                          <span class="day">29</span>
                          <span class="month">May</span>
                        </time>
                        <header class="heading-container">
                          <h4 class="title">Suppama Hiit Instructor Training Bergen, Norway</h4>
                          <div class="plus"></div>
                        </header>
                      </div>
                    </section>
                  </li>
                  <li>
                    <section class="event">
                      <picture class="image-block">
                        <img src="multimedia/images/medium/m-person-2.jpg" alt="">
                      </picture>
                      <div class="text-block">
                        <time class="date" datetime="2018-05-30">
                          <span class="day">30</span>
                          <span class="month">May</span>
                        </time>
                        <header class="heading-container">
                          <h4 class="title">Suppama Hiit Instructor Training Bergen, Norway</h4>
                          <div class="plus"></div>
                        </header>
                      </div>
                    </section>
                  </li>
                  <li>
                    <section class="event">
                      <picture class="image-block">
                        <img src="multimedia/images/medium/m-person-2.jpg" alt="">
                      </picture>
                      <div class="text-block">
                        <time class="date" datetime="2018-06-01">
                          <span class="day">01</span>
                          <span class="month">Jun</span>
                        </time>
                        <header class="heading-container">
                          <h4 class="title">Suppama Hiit Instructor Training Bergen, Norway</h4>
                          <div class="plus"></div>
                        </header>
                      </div>
                    </section>
                  </li>
                  <li>
                    <section class="event">
                      <picture class="image-block">
                        <img src="multimedia/images/medium/m-person-2.jpg" alt="">
                      </picture>
                      <div class="text-block">
                        <time class="date" datetime="2018-06-02">
                          <span class="day">02</span>
                          <span class="month">Jun</span>
                        </time>
                        <header class="heading-container">
                          <h4 class="title">Suppama Hiit Instructor Training Bergen, Norway</h4>
                          <div class="plus"></div>
                        </header>
                      </div>
                    </section>
                  </li>
                </ul>
                <div class="button-container">
                  <button type="button" name="events-all">
                    <span class="content-container">
                      <picture class="side-image">
                        <img src="multimedia/images/x-small/S-calendar-1.png" alt="">
                      </picture>
                      <span class="call">See All</span>
                    </span>
                  </button>
                </div>
              </div>
            </section>
            <section class="quality">
              <header class="heading-container">
                <div class="circle">
                  <picture class="ilustration">
                    <img src="multimedia/images/x-small/S-prize-1.png" alt="">
                  </picture>
                </div>
                <h3 class="title">Quality</h3>
                <div class="decorative-line"></div>
              </header>
              <div class="content-body">
                <div class="text-block">
                  <h4 class="title">Best Quality Products</h4>
                  <div class="text-set">
                    <p class="text">
                      Ac nesciunt eleifend! Maxime dicta laboris? Explicabo, mattis dictum incididunt dolorem? Exercitationem aliquip sagittis.
                    </p>
                  </div>
                </div>
                <picture class="product-image">
                  <img src="multimedia/images/medium/m-graph-1.png" alt="">
                </picture>
              </div>
            </section>
          </div>
        </div>
      </section>
    </main>
    <footer id="top-footer">
      <section class="general-info">
        <div class="content-container">
          <div class="logo-container">
            <picture class="logo">
              <img src="multimedia/images/small/s-logo-1.png" alt="">
            </picture>
            <span class="message">Copyright &copy; 2018 &ndash; Suppama &ndash; All rights reserved</span>
          </div>
          <div class="info-container">
            <nav class="sitemap">
              <h4 class="title">Sitemap</h4>
              <ul class="pages-list">
                <li><a href="#">Indoor</a></li>
                <li><a href="#">Outdoor</a></li>
                <li><a href="#">Training</a></li>
                <li><a href="#">Products</a></li>
                <li><a href="#">Be a part of Suppama</a></li>
                <li><a href="#">Contact Us</a></li>
              </ul>
            </nav>
            <section class="contact-us">
              <h4 class="title">Contact Us</h4>
              <address class="contact-info">
                <span class="line">Falbrock OÜ</span>
                <dl class="line"><dt>Reg nr</dt> <dd>12026140</dd></dl>
                <span class="line">Tammepöllu tee 17</span>
                <span class="line">Viimsi 74001</span>
                <dl class="line"><dt>E-mail</dt> <dd>info@suppama.com</dd></dl>
                <dl class="line"><dt>Phone</dt> <dd>+372 5858 0905</dd></dl>
              </address>
            </section>
          </div>
          <div class="content-set">
            <form name="footer-contact-form" method="post" autocomplete="on">
              <header class="heading-container">
                <h1 class="title">Join Our List For Updates</h1>
                <picture class="decorative-border">
                  <img src="multimedia/images/small/s-border-1.png" alt="">
                </picture>
              </header>
              <input name="footer-email" type="email" placeholder="Enter your e-mail">
            </form>
            <ul class="social-list">
              <li><a href="#" class="facebook"><img src="multimedia/images/x-small/S-facebook-2.png" alt="Facebook"></a></li>
              <li><a href="#" class="twitter"><img src="multimedia/images/x-small/S-twitter-2.png" alt="Twitter"></a></li>
              <li><a href="#" class="youtube"><img src="multimedia/images/x-small/S-youtube-1.png" alt="Youtube"></a></li>
              <li><a href="#" class="linkedin"><img src="multimedia/images/x-small/S-linkedin-1.png" alt="Linkedin"></a></li>
            </ul>
          </div>
        </div>
      </section>
      <section class="copyright">
        <div class="content-container">
          <nav class="copyright-info">
            <ul class="links-list">
              <li><a href="#">Terms of Use</a></li>
              <li><a href="#">Privacy Policy</a></li>
            </ul>
            <div class="copyright-container">
              <span class="message">&copy; 2001&ndash;2018 Suppama.com &ndash; All Rights Reserved</span>
            </div>
          </nav>
          <div class="cards-set">
            <picture class="payment-card">
              <img src="multimedia/images/x-small/S-payment-card-1.png" alt="">
            </picture>
            <picture class="payment-card">
              <img src="multimedia/images/x-small/S-payment-card-2.png" alt="">
            </picture>
            <picture class="payment-card">
              <img src="multimedia/images/x-small/S-payment-card-3.png" alt="">
            </picture>
            <picture class="payment-card">
              <img src="multimedia/images/x-small/S-payment-card-4.png" alt="">
            </picture>
          </div>
        </div>
      </section>
    </footer>
  </body>
</html>
