Object.defineProperties( Array.prototype,
  { oSelectForEach: // Itera um arranjo de elementos para retornar um arranjo com os elementos abarcados pela dada string de seleção
    { value: function oSelectForEach( selector, target = "children", count = 1 )
        { if( !this.every( element => element instanceof Element ) ) throw new RangeError( "Just arrays with only elements are allowed by oSelectForEach." );
          if( typeof selector != "string" ) throw new TypeError( "The selector argument of oSelectForEach must be a string." );
          if( count <= 0 ) throw new RangeError( "The count argument of oSelectForEach must be greater than 0." );
          var selection = [], method;
          switch( target )
            { case "children": method = count > 1 ? "querySelectorAll" : "querySelector"; break;
              case "parents": method = "closest"; break;
              default: throw new RangeError( "The allowed values for target argument of oSelectForEach are 'children' and 'parents'." ) };
          switch( method )
            { case "querySelector": this.forEach( element =>
                { selection = selection.concat( element.querySelector( selector ) || [] ) } ); break;
              case "querySelectorAll": this.forEach( element =>
                { selection = selection.concat( Array.from( element.querySelectorAll( selector ) ).slice( 0, count ) ) } ); break;
              case "closest": this.forEach( element =>
                { for( let actualElement = element.parentElement, i = 0; ( actualElement = actualElement.closest( selector ) ) && i < count; i++ )
                    { selection.push( actualElement ); actualElement = actualElement.parentElement } } ) }
          return selection },
      enumerable: true } } );

Object.defineProperties( Node.prototype,
  { oChangeBySize: // Executa uma função ou retorna um boolean segundo o tamanho de dado objeto
      { value: function oChangeBySize( media, truthFunction, falsyFunction, evaluedNode )
          { var targetComponent;
            let acceptedMediaKeys = [ [ "innerWidth", "innerHeight", "outerWidth", "outerHeight" ],
                                      [ "width", "height", "availWidth", "availHeight" ],
                                      [ "clientWidth", "clientHeight", "offsetWidth", "offsetHeight", "scrollWidth", "scrollHeight" ] ];
            for( let size in media )
              { if( typeof media[ size ] !== "number" ) throw new TypeError( "Object value in media argument of oChangeBySize must be a number." );
                let sizeID;
                for( let i = 0; i < acceptedMediaKeys.length; i++ )
                  { if( acceptedMediaKeys[i].includes( size ) ) { sizeID = i; break } }
                switch( sizeID )
                  { case 0: targetComponent = window; break;
                    case 1: targetComponent = window.screen; break;
                    case 2: targetComponent = evaluedNode || this; break;
                    default: throw new RangeError( "Object key in media argument of oChangeBySize function must be a window, screen or element size property." ) }
                if( targetComponent[ size ] > media[ size ] ) return falsyFunction ? falsyFunction() : false }
            return truthFunction ? truthFunction() : true },
        enumerable: true },
    oChangePlacementBySize: // Realiza a inserção do alvo em um receptáculo em função do tamanho de dado objeto
      { value: function oChangePlacementBySize( media, receiver, nextSibling, evaluedNode )
          { let originalParent = this.originalParent = this.originalParent || this.parentElement,
                originalSibling = this.originalSibling = this.originalSibling || this.nextElementSibling || "none";
            if( this.oChangeBySize( media, false, false, evaluedNode || receiver ) )
              { if( !Array.from( receiver.children ).includes( this ) ) nextSibling ? receiver.insertBefore( this, nextSibling ) : receiver.appendChild( this ) }
            else
              { if( Array.from( receiver.children ).includes( this ) ) originalSibling != "none" ? originalParent.insertBefore( this, originalSibling ) : originalParent.appendChild( this ) } },
        enumerable: true } } );
