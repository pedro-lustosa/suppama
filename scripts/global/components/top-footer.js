/* Identificadores */
export const topFooter = document.getElementById( "top-footer" );
topFooter:
  { var logoContainer = topFooter.logoContainer = topFooter.querySelector( ".logo-container" );
    var generalInfo = topFooter.generalInfo = topFooter.querySelector( ".general-info" );
    generalInfo:
      { var infoContainer = generalInfo.infoContainer = generalInfo.querySelector( ".info-container" );
        infoContainer:
          { var sitemap = infoContainer.sitemap = infoContainer.querySelector( ".sitemap" );
            var contactUs = infoContainer.contactUs = infoContainer.querySelector( ".contact-us" ) }
        var contentSet = generalInfo.contentSet = generalInfo.querySelector( ".content-set" ) } }

export const media = { maxWidth: [ 1199, 849 ] };

/* Ouvintes de Evento */
changePlacement:
  { let targets = { logoContainer: [ media.maxWidth[ 1 ], sitemap ], contentSet: [ media.maxWidth[ 0 ], contactUs ] };
    for( let target in targets )
      { window.addEventListener( "resize", () => eval( target ).oChangePlacementBySize( { innerWidth: targets[ target ][0] }, infoContainer, targets[ target ][1] ) );
        eval( target ).oChangePlacementBySize( { innerWidth: targets[ target ][0] }, infoContainer, targets[ target ][1] ) } }
