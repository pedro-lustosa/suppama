/* Identificadores */
const products = document.getElementById( "products" );
products:
  { var categories = products.categories = Array.from( products.getElementsByClassName( "product-category" ) );
    categories:
      { var categoriesHeadings = categories.headings = categories.oSelectForEach( ".heading-container" );
        var categoriesButtons = categories.buttons = categoriesHeadings.oSelectForEach( "button[ name$='all' ]" ) } }

const media = { maxWidth: [ 1249 ] };

/* Ouvintes de Evento */
changePlacement:
  { let buttons = categoriesButtons;
    for( let button of buttons )
      { window.addEventListener( "resize", () => button.oChangePlacementBySize( { innerWidth: media.maxWidth[0] }, categories[ buttons.indexOf( button ) ] ) );
        button.oChangePlacementBySize( { innerWidth: media.maxWidth[0] }, categories[ buttons.indexOf( button ) ] ) } }
